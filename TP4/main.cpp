
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"

#include <iostream>
#include <vector>

int main(int argc, char* argv[]){
    Ligne ligne({1, 1, 1}, {5, 5}, {5, 5});
    ligne.afficher();

    PolygoneRegulier polygone({0, 1, 0}, {100, 200}, 50, 5);
    polygone.afficher();

    std::vector<FigureGeometrique*>  _figures{
        new Ligne({1, 1, 1}, {5, 5}, {5, 5}),
        new PolygoneRegulier({0, 1, 0}, {100, 200}, 50, 5)
    };

    for (FigureGeometrique * _ptrObjet : _figures){
            _ptrObjet->afficher();
    }
    return 1;
}