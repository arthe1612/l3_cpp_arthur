#include "PolygoneRegulier.hpp"
#include <cmath>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : FigureGeometrique(couleur){
    double angRads = 2 * M_PI / nbCotes;
    for (int i = 0 ; i < nbCotes ; i++){
        _points.push_back({centre._x + rayon * sin(i * (int)angRads), centre._y + rayon * cos(i * (int)angRads)});
    }
}

void PolygoneRegulier::afficher() const{
    std::cout << "Polygone regulier " << getCouleur()._r  << "_" << getCouleur()._g << "_" << getCouleur()._b << std::endl;
    for (auto& i : _points){
        std::cout << "  -> " << i._x << "_" << i._y << " " << std::endl;
    }
}

int PolygoneRegulier::getNbPoints() const {
    return _points.size();
}

const Point & PolygoneRegulier::getPoint(int indice) const {
    return _points[indice];
}