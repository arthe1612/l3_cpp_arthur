#ifndef DEF_POLYGONEREGULIER
#define DEF_POLYGONEREGULIER
#include <vector>
#include "FigureGeometrique.hpp"

class PolygoneRegulier : public FigureGeometrique{
    private:
        int _nbPoints;
        std::vector<Point>  _points;
    public:
        PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes);
        void afficher(const Cairo::RefPtr<Cairo::Context> & cr) const override;
        int getNbPoints() const ;
        const Point & getPoint(int indice) const;
};

#endif