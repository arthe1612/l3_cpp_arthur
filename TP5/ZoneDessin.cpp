#include "ZoneDessin.hpp"
#include <iostream>


ZoneDessin::ZoneDessin() : rng(std::random_device()()), _cotes(3, 20), _taille(10, 100){
   add_events(Gdk::BUTTON_PRESS_MASK);
}

ZoneDessin::~ZoneDessin(){
    _figures.clear();
}

bool ZoneDessin::on_draw(const Cairo::RefPtr<Cairo::Context> & cr){
    for ( int i = 0; i < _figures.size(); i++)
    {
        _figures[i]->afficher(cr);
    }
    return true;
}

bool ZoneDessin::gererClic(){

}

bool ZoneDessin::on_button_press_event(GdkEventButton* events) {
    if(events->button == 1){
        _figures.push_back(new PolygoneRegulier({0, 0, 1}, {events->x, events->y}, _taille(_rand), _cotes(_rand)));
    }else if(events->button == 3){
        _figures.pop_back();
    }

    get_window()->invalidate(true);
    auto window = get_window();
    return true;
}