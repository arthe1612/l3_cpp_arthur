#ifndef DEF_VIEWER_FIGURES
#define DEF_VIEWER_FIGURES

#include <gtkmm.h>
#include "ZoneDessin.hpp"

class ViewerFigures{
    protected:
        Gtk::Main _kit;
        Gtk::Window _window;
        ZoneDessin _zoneDessin;
    public:
        ViewerFigures(int argc, char** argv);
        void run();
};
#endif