#ifndef DEF_ZONE_DESSIN
#define DEF_ZONE_DESSIN

//#include <gtkmm.h>
#include <vector>

#include "FigureGeometrique.hpp"
#include "PolygoneRegulier.hpp"
#include "Ligne.hpp"
#include <random>

class ZoneDessin : public Gtk::DrawingArea{

    protected:
        std::vector<FigureGeometrique*> _figures;
        Gtk::Button _button;
        std::random_device _rand;
        std::mt19937 rng;
        std::uniform_int_distribution<std::mt19937::result_type> _cotes; // distribution in range [1, 6]
        std::uniform_int_distribution<std::mt19937::result_type> _taille;

    public:
        ZoneDessin();
        ~ZoneDessin();
        bool on_draw(const Cairo::RefPtr<Cairo::Context> & cr) override;
        bool gererClic();
        bool on_button_press_event(GdkEventButton* event) override;

};
#endif