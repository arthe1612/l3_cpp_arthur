#include "Ligne.hpp"
#include <iostream>

Ligne::Ligne(const Couleur & couleur, const Point & p0, const Point & p1) : FigureGeometrique(couleur){
    _couleur = couleur;
    _p0 = p0;
    _p1 = p1;
}

void Ligne::afficher(const Cairo::RefPtr<Cairo::Context> & cr) const{
    cr->set_line_width(5.0);
    cr->set_source_rgb(0.8, 0.0, 0.0);
    cr->move_to(_p0._x, _p0._y);
    cr->line_to(_p1._x, _p1._y);
    cr->stroke();
}

const Point & Ligne::getP0() const{
    return _p0;
}
const Point & Ligne::getP1() const{
    return _p1;
}