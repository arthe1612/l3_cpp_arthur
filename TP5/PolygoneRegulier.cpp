#include "PolygoneRegulier.hpp"
#include <cmath>
#include <iostream>

PolygoneRegulier::PolygoneRegulier(const Couleur & couleur, const Point & centre, int rayon, int nbCotes) : FigureGeometrique(couleur){
    double angRads = 2 * M_PI / nbCotes;
    for (int i = 0 ; i < nbCotes ; i++){
        _points.push_back({centre._x + rayon * sin(i * angRads), centre._y + rayon * cos(i * angRads)});
    }
    _nbPoints = getNbPoints();
}

void PolygoneRegulier::afficher(const Cairo::RefPtr<Cairo::Context> & cr) const{
    cr->set_line_width(5.0);
    cr->set_source_rgb(0, 0.8, 0.0);
    for (int i = 0 ; i < _nbPoints - 1 ; i++){
        cr->move_to(_points[i]._x, _points[i]._y);
        cr->line_to(_points[i + 1]._x, _points[i + 1]._y);
    }
    cr->move_to(_points[_nbPoints-1]._x, _points[_nbPoints-1]._y);
    cr->line_to(_points[0]._x, _points[0]._y);
    cr->stroke();
}

int PolygoneRegulier::getNbPoints() const {
    return _points.size();
}

const Point & PolygoneRegulier::getPoint(int indice) const {
    return _points[indice];
}