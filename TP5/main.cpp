
#include "Ligne.hpp"
#include "PolygoneRegulier.hpp"
#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

#include <iostream>
#include <vector>

int main(int argc, char* argv[]){

    ViewerFigures _viewFigures(argc, argv);
    _viewFigures.run();

    return 1;
}