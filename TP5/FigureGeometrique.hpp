#ifndef DEF_FIGUREGEOMETRIQUE
#define DEF_FIGUREGEOMETRIQUE
#include "Couleur.hpp"
#include "Point.hpp"
#include <gtkmm.h>

class FigureGeometrique{
    protected:
        Couleur _couleur;
    public:
        FigureGeometrique();
        FigureGeometrique(const Couleur & couleur);
        const Couleur & getCouleur() const;
        virtual void afficher(const Cairo::RefPtr<Cairo::Context> & cr) const = 0;
};

#endif