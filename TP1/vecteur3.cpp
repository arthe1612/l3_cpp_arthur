#include "vecteur3.hpp"
#include<iostream>
#include <math.h>

vecteur3::vecteur3(float x, float y, float z){
    vecteur.x = x;
    vecteur.y = y;
    vecteur.z = z;
}

void vecteur3::afficherVecteur(){
    std::cout << "x ->" << vecteur.x << " y ->" << vecteur.y << " z ->" << vecteur.z << "." << std::endl;
}

float vecteur3::calculerNorme(){
    return sqrt(pow(vecteur.x, 2) + pow(vecteur.y, 2) + pow(vecteur.z, 2));
}

float vecteur3::produitScalaire(vecteur3 vecteur_a, vecteur3 vecteur_b){
    return vecteur_a.vecteur.x*vecteur_b.vecteur.x + vecteur_a.vecteur.y*vecteur_b.vecteur.y + vecteur_a.vecteur.z*vecteur_b.vecteur.z;
}

vecteur3 vecteur3::addition(vecteur3 vecteur_a, vecteur3 vecteur_b){
    return vecteur3(vecteur_a.vecteur.x + vecteur_b.vecteur.x, vecteur_a.vecteur.y + vecteur_b.vecteur.y, vecteur_a.vecteur.z + vecteur_b.vecteur.z);
}
