#include "fibonacci.hpp"
#include "vecteur3.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupDoubler) { };

TEST(GroupDoubler, test_fibonacci_Iteratif) {  // premier test
    int result = 0;

    result = fibonacciIteratif(1);
    CHECK_EQUAL(1, result);

    result = fibonacciIteratif(2);
    CHECK_EQUAL(1, result);

    result = fibonacciIteratif(3);
    CHECK_EQUAL(2, result);

    result = fibonacciIteratif(4);
    CHECK_EQUAL(3, result);

    result = fibonacciIteratif(5);
    CHECK_EQUAL(5, result);
}

TEST(GroupDoubler, test_fibonacci_Recursif) {  // deuxième test
    int result = 0;

    result = fibonacciRecursif(1);
    CHECK_EQUAL(1, result);

    result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);

    result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);

    result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);

    result = fibonacciRecursif(5);
    CHECK_EQUAL(5, result);
}


TEST(GroupDoubler, test_produit_scalaire){ //Test vecteur
    float result = 0 ;
    vecteur3 vecteur_a(5.6, 7, 10.9);
    vecteur3 vecteur_b(1.5, 6.4, 9);

    result = vecteur_a.produitScalaire(vecteur_a, vecteur_b);

    CHECK_EQUAL(151.3, result);


}
