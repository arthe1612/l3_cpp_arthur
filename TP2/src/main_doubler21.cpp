#include "Doubler.hpp"

#include <iostream>

int main() {
    int * t;
    t = new int[10];
    t[3] = 42;
    std::cout << t[3] << std::endl;
    
    delete []t;
    t = nullptr;
}
