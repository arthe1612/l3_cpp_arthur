#include "Bibliotheque.hpp"
#include <iostream>

int main() {
    Bibliotheque _bibliotheque();
    Livre l1("t1", "a1", 1);
    Livre l2("t2", "a2", 1);
    std::cout << (l1 < l2) << std::endl;
    std::cout << (l1 == l2) << std::endl;
    std::cout << "blabla" << std::endl;
    return 0;
}