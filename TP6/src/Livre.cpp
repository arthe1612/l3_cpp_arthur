#include "Livre.hpp"

Livre::Livre(){

}

Livre::Livre(const std::string & titre, const std::string & auteur, int annee) : _titre(titre), _auteur(auteur), _annee(annee) {
    if(_titre.find("\n") != std::string::npos){
        throw std::string("erreur : titre non valide ('\n' non autorisé)");
    }

    else if(_auteur.find("\n") != std::string::npos){
        throw std::string("erreur : auteur non valide ('\n' non autorisé)");
    }

    else if(_titre.find(";") != std::string::npos){
        throw std::string("erreur : titre non valide (';' non autorisé)");
    }

    else if(_auteur.find(";") != std::string::npos){
        throw std::string("erreur : auteur non valide (';' non autorisé)");
    }
}

const std::string & Livre::getTitre() const{
    return std::string("titre1");
}

const std::string & Livre::getAuteur() const{
    return std::string("auteur1");
}

int Livre::getAnnee() const{
    return 1998;
}

bool Livre::operator<(const Livre &l2) const{
    if(_auteur < l2._auteur)
        return true;
    else
        return _auteur == l2._auteur && _titre < l2._titre;
}

bool operator==(const Livre & l1, const Livre & l2){
    return l1.getAuteur() == l2.getAuteur()
       && l1.getTitre() == l2.getTitre();
}