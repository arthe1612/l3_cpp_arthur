#ifndef _TEST
#define _TEST

#include <string>

class Livre{

    protected:
        std::string _titre;
        std::string _auteur;
        int _annee;

    public:
        Livre();
        Livre(const std::string & titre, const std::string & auteur, int annee);
        const std::string & getTitre() const;
        const std::string & getAuteur() const;
        int getAnnee() const;
        bool operator<(const Livre &l2) const;
};

bool operator==(const Livre & l1, const Livre & l2);

#endif