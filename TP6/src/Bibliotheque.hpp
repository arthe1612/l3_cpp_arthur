#include <vector>
#include "Livre.hpp"
#ifndef _TEST2
#define _TEST2

class Bibliotheque : public std::vector<Livre>{
    public:
        void afficher() const;
        void trierParAuteurEtTitre();
        void trierParAnnee();
        void lireFichier(const std::string & nomFichier);
        void ecrireFichier(const std::string & nomFichier) const;
};

#endif