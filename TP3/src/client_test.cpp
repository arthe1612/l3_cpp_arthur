#include "Client.hpp"
#include <iostream>
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupClient) { };

TEST(GroupClient, client_test)  {
    Client monClient = Client(5, "arthur");
    CHECK_EQUAL(monClient.getId(),5);
    CHECK_EQUAL(monClient.getNom(),"arthur");
}
