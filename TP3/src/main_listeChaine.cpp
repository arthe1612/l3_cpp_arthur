#include "Liste/Liste.hpp"

#include <iostream>

int main() {
    Liste maListe;
    
    maListe.ajouterDevant(13);
    maListe.ajouterDevant(37);
    for(int i = 0 ; i < maListe.getTaille(); i++){
		std::cout << maListe.getElement(i) << std::endl;
	}

	return 0;
}
