#include "Liste/Liste.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupListe) { };

TEST(GroupListe, Liste_test1)  {
    Liste l;
    CHECK(l._tete == nullptr);
}

TEST(GroupListe, Liste_test2)  {
    Liste l;
    l.ajouterDevant(37);
    CHECK(l._tete != nullptr);
    CHECK(l._tete->_valeur == 37);
    CHECK(l._tete->_suivant == nullptr);
}


TEST(GroupListe, Liste_test3)  {
    Liste l;
    l.ajouterDevant(37);
    l.ajouterDevant(725);
    l.ajouterDevant(14);
    l.ajouterDevant(0);
    l.ajouterDevant(98);
    CHECK(l.getElement(0) == 98);
    CHECK(l.getElement(1) == 0);
    CHECK(l.getElement(2) == 14);
    CHECK(l.getElement(3) == 725);
    CHECK(l.getElement(4) == 37);
}
