#include <iostream>
#include "Liste.hpp"

Liste::Liste():_tete(nullptr){}

void Liste::ajouterDevant(int valeur){
	_tete = new Noeud{valeur, _tete};
}

int Liste::getElement(int indice) const{
	Noeud * noeud = _tete;
	int i = 0;
	
	while((i < indice) && (noeud != nullptr))
	{
		noeud = noeud->_suivant;
		i++;
	}
	return noeud->_valeur;
}

int Liste::getTaille() const{
	int i = 0;
	Noeud* noeud = _tete;
	if(noeud != nullptr){
		do{
			noeud = noeud->_suivant;
			i++;
		}while(noeud != nullptr);
	}
	else{
		return i;
	}
	return i;
}


Liste::~Liste(){
	while(_tete){
		Noeud * sup = _tete;
		_tete = _tete->_suivant;
		delete sup;
	}
}
