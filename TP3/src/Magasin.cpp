#include "Magasin.hpp"
#include <iostream>
#include <algorithm>

Magasin::Magasin()
{
    _idCourantClient = 0;
}


int Magasin::nbClients() const{
    return _idCourantClient;
}

void Magasin::ajouterClient(const std::string & nom){
    _idCourantClient ++ ;
    _clients.push_back(Client(_idCourantClient, nom));
}

void Magasin::afficherClients(){
    std::cout << "Liste des clients : " <<std::endl;
    for (auto& i : _clients){
        std::cout << "Client n° " << i.getId() << " Nom -> " << i.getNom() <<std::endl;
    }
}

void Magasin::supprimerCLient(int idClient){
    for (auto& i : _clients){
        if(i.getId() == idClient ){
            std::swap(i, _clients.back());
            _clients.pop_back();
        }
    }
}
