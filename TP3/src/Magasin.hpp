#ifndef MAGASIN_HPP
#define MAGASIN_HPP
#include "Client.hpp"
#include "Produit.hpp"
#include "location.hpp"
#include<vector>
#include <iostream>
#include <string>

class Magasin
{
    private:
        std::vector<Client> _clients;
        std::vector<Produit> _produits;
        std::vector<location> _locations;
        int _idCourantClient;
        int _idCourantProduit;

    public:
        Magasin();
        int nbClients() const;
        void ajouterClient(const std::string & nom);
        void afficherClients();
        void supprimerCLient(int idClient);
};

#endif // MAGASIN_HPP
