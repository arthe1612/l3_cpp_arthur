#include "location.hpp"
#include <iostream>
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupLocation){};

TEST(GroupLocation, location_test)  {
    location maLocation {42, 37};
    CHECK_EQUAL(maLocation._idClient,42);
    CHECK_EQUAL(maLocation._idProduit,37);
}
