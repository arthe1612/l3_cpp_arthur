#include "Produit.hpp"

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProduit) { };

TEST(GroupProduit, produit_test)  {
    Produit monClient = Produit(4, "jambon");
    CHECK_EQUAL(monClient.getId(),4);
    CHECK_EQUAL(monClient.getDescription(),"jambon");
}
