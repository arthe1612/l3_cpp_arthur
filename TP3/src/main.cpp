#include "location.hpp";
#include "Magasin.hpp";

#include <iostream>;

int main() {
    location maLocation {42, 37};
    Client monClient = Client(5, "arthur");
    Produit monProduit = Produit(4, "jambon");

    Magasin monMagasin = Magasin();
    monMagasin.ajouterClient(monClient.getNom());
    std::cout << "Nombre de clients : " << monMagasin.nbClients() << std::endl;

    monMagasin.afficherClients();


    monClient.afficherClient();
    maLocation.afficherLocation();
    monProduit.afficherProduit();
    return 0;
}
