#ifndef _ETUDIANT
#define _ETUDIANT

#include "Personne.hpp"
class Etudiant : public Personne{
    private:
        float _moyenne;

    public:
        Etudiant(const std::string & nom, float moyenne);
        void afficher() const override;
        float getMoyenne() const;

};

#endif