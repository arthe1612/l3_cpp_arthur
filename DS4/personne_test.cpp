#include "Personne.hpp"
#include <iostream>
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupPersonne){};

TEST(GroupPersonne, personne_test)  {
    Personne personne("Julien");

    CHECK(personne.getNom() == "Julien");
}
