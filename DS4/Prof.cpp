#include "Prof.hpp"
#include <iostream>

Prof::Prof(const std::string & nom, float salaire) : Personne(nom){
    _salaire = salaire;
}

void Prof::afficher() const {
    std::cout << "Professeur de nom b" << _nom << " et de salaire "<< _salaire << std::endl;
}

float Prof::getSalaire() const{
    return _salaire;
}