#ifndef _PERSONNE
#define _PERSONNE

#include <iostream> 
#include <string>

class Personne{
    protected:
        std::string _nom;

    public:
        Personne(const std::string & nom);
        virtual void afficher() const;
        std::string getNom() const;
};

#endif