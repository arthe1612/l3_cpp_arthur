#include "Personne.hpp"

Personne::Personne(const std::string & nom){
    _nom = nom;
}

void Personne::afficher() const{
    std::cout << "Personne de nom " << _nom <<std::endl;
}

std::string Personne::getNom() const{
    return _nom;
}