#include "Etudiant.hpp"
#include <iostream>
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupEtudiant){};

TEST(GroupEtudiant, etudiant_test)  {
    Etudiant etudiant("Arthur", 20);

    CHECK(etudiant.getMoyenne() == 20);
    CHECK(etudiant.getNom() == "Arthur")
}
