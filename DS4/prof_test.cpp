#include "Prof.hpp"
#include <iostream>
#include <string>

#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupProf){};

TEST(GroupProf, prof_test)  {
    Prof prof("Julien", 1000);

    CHECK(prof.getSalaire() == 1000);
    CHECK(prof.getNom() == "Julien")
}
