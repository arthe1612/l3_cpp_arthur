#include <iostream>
#include "Etudiant.hpp"
#include "Prof.hpp"
#include <vector>

int main(int argc, char* argv[]){
    
    std::vector<Personne *> _personnes{
        new Personne("arthur"),
        new Etudiant("toto", 25),
        new Prof("julien", 10000.5)
    };

    for(Personne* _personne : _personnes){
        _personne->afficher();
    }
    return 1;
}