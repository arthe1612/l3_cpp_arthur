#ifndef _PROF
#define _PROF

#include <string>
#include <iostream>
#include "Personne.hpp"

class Prof : public Personne{
    private:
        float _salaire;

    public:
        Prof(const std::string & nom, float salaire);
        void afficher() const override;
        float getSalaire() const;

};


#endif