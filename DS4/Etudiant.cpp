#include "Etudiant.hpp"
#include <iostream>

Etudiant::Etudiant(const std::string & nom, float moyenne) : Personne(nom){
    _moyenne = moyenne;
}

void Etudiant::afficher() const {
    std::cout << "Etudiant de nom " << _nom << " et de moyenne " << _moyenne <<std::endl;
}

float Etudiant::getMoyenne() const{
    return _moyenne;
}